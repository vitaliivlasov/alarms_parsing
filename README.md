## Python script for parsing cloutwatch alarms
This script generates a csv file with the following properties:

* Name
* Description
* Conditions
* Sns topic(s)
* Metric name

To work you need:

* aws cli
* python 3.8+

How to use:

    $ aws cloudwatch describe-alarms > input.json
    $ python3 main.py -f input.json

In the current directory, a report is created with the name "alarms-yyyy-mm-dd hh-mm-ss.ms.csv"

It is possible to specify additional properties from the list:

* AlarmArn
* ActionsEnabled
* OKActions
* InsufficientDataActions
* StateValue
* Namespace
* Statistic

How to use:

    $ python3 main.py -f input.json -e "AlarmArn StateValue"


**Warning!** Additional properties can contain an empty value, but this does not mean that they are not specified! Check the properties in the AWS Management Console.