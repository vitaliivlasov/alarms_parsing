import csv
import json
import os
import re
from datetime import datetime
import logging
import argparse


logging.basicConfig(level=logging.DEBUG, format='%(levelname)s - %(message)s')

comparison_signs = {
    'GreaterThanOrEqualToThreshold': '>=',
    'GreaterThanThreshold': '>',
    'LessThanThreshold': '<',
    'LessThanOrEqualToThreshold': '<=',
    'LessThanLowerOrGreaterThanUpperThreshold': 'LessThanLowerOrGreaterThanUpperThreshold',
    'LessThanLowerThreshold': '<',
    'GreaterThanUpperThreshold': '>',
}
intervals = (
    ('hours', 3600),
    ('minutes', 60),
    ('seconds', 1),
)
extra_values = ['AlarmArn', 'ActionsEnabled', 'OKActions', 'InsufficientDataActions',
                'StateValue', 'Namespace', 'Statistic']


parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", help='File name for parsing')
parser.add_argument("-e", "--extra", help='Specify additional properties separated by a space -e="AlarmArn OKActions"')
args = parser.parse_args()

if args.file:
    input_file = args.file

if args.extra:
    values = args.extra.split()
    for val in values:
        if val not in extra_values:
            logging.error(f'This property "{val}" does not exist or is not supported yet!')
            exit(1)

filename = f'alarms-{datetime.now()}.csv'
headers = ['Name', 'Description', 'Conditions', 'Sns topic(s)', 'Metric name']
if args.extra:
    headers.extend(values)
with open(filename, 'w', encoding='UTF8') as f:
    writer = csv.writer(f)
    # write the header
    writer.writerow(headers)


def display_time(seconds, granularity=2):
    result = []

    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{} {}".format(value, name))
    return ', '.join(result[:granularity])


try:
    with open(input_file, 'r') as f:
            data = json.load(f)
except (NameError, FileNotFoundError):
    logging.error('File not specified or specified incorrectly!')
    os.remove(filename)
    exit(1)

data = data.get('MetricAlarms')
count = 0

for item in data:
    name = item.get('AlarmName')
    description = item.get('AlarmDescription', '')
    if description and description[0] == '{' and description[-1] == '}':
        description = "".join(description.split())

    alarm_actions = item.get('AlarmActions')
    if alarm_actions:
        alarm_actions_string = ', '.join(alarm_actions)
    else:
        alarm_actions_string = ''
    metric_name = item.get('MetricName', '')


    metrics = item.get('Metrics')
    sign = comparison_signs.get(item.get('ComparisonOperator'))
    threshold = item.get('Threshold')
    if str(threshold)[-1] == '0':
        threshold = int(threshold)

    datapoints = item.get('DatapointsToAlarm')
    if not datapoints:
        datapoints = item.get('EvaluationPeriods')
    max_datapoints = item.get('EvaluationPeriods')
    period = ''

    if metrics:
        first_metric = metrics[0]
        label = first_metric.get('Label')

        if not label:
            for metric in metrics:
                label = metric.get('MetricStat').get('Metric').get('MetricName')
                if label:
                    label = label
                    break

        for metric in metrics:
            if metric.get('MetricStat'):
                period = metric.get('MetricStat').get('Period') * max_datapoints
                break
        period = display_time(period)

        condition = f'{label} {sign} {threshold} for {datapoints} datapoints within {period}'

        if not threshold:
            for metric in metrics:
                expression = metric.get('Expression')
                if expression and ('ANOMALY_DETECTION_BAND' in expression):
                    threshold = re.search(r'(\d+)\D+$', expression).group(1)
                    break
            condition = f'{label} {sign} the band (width: {threshold}) for {datapoints} datapoints within {period}'

    else:
        label = item.get('MetricName')
        period = item.get('Period') * max_datapoints
        period = display_time(period)
        condition = f'{label} {sign} {threshold} for {datapoints} datapoints within {period}'

    write_data = [name, description, condition, alarm_actions_string, metric_name]

    if args.extra:
        for val in values:
            if val in ('OKActions', 'InsufficientDataActions'):
                write_data.append(', '.join(item.get(val)))
            else:
                write_data.append(item.get(val, ''))

    with open(filename, 'a', encoding='UTF8') as f:
        writer = csv.writer(f)
        writer.writerow(write_data)
    count += 1

if count == 0:
    logging.error('The file is empty. Check if there are alarms in the region!')
    os.remove(filename)
else:
    logging.info(f'Added {count} records in the file {filename}')
